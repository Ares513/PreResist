<!DOCTYPE html>
<html>


<head>
<title>PreResist.org</title>

</head>
<?php
include('../../db_settings.php');
$link = mysqli_connect("50.62.209.38:3306", $db_user, $db_pass)
or die("Could not connect: " . mysqli_error());

unset($db_user, $db_pass);
include('../../db_queries.php');
$id = serve_one_unapproved($link);
echo_one_img_by_id($link, $id);
echo '<body class="w3-content" style="max-width:1300px">';
echo '<br>';
echo count_approved($link);
echo ' are approved and ';
echo count_unapproved($link);
echo ' are awaiting approval. You are viewing id ';
echo $id;
echo '<br>';
echo '<form action="review.php" enctype="multipart/form-data" method="POST">';
echo '<input type="submit" name="action" value="Approve" /> Yes, this is a legitimate image.';
echo '<br>';
echo '<input type="submit" name="action" value="Defer" /> Defer this to another reviewer.';
echo '<br>';
echo '<input type="submit" name="action" value="Disapprove" /> This has no value and should be deleted.';
echo '<input type="hidden" name="id" value="';
echo $id; 
mysqli_close($link);
?>">
</form>
</body>
</html>
